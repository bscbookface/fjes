#!/usr/bin/env python
# -*- coding: utf-8 -*-
""" Simple web application
This application is built using Flask and PyMongo. It will query the database
and present the data on a simple website using a template.
"""

# Import
import yaml
from pymongo import MongoClient, errors
from flask import Flask, render_template


# Define
app = Flask(__name__)
maxSevSelDelay = 3000

# Config file setup
print " * Configuration file setup..."
with open("config.yml", 'r') as ymlfile:
    cfg = yaml.load(ymlfile)

# Database setup
print " * Database setup..."
try:
    dbCon = MongoClient('mongodb://' + cfg['databaseIP'] + ':' + \
	    cfg['databasePort'] + '/', serverSelectionTimeoutMS=maxSevSelDelay)
    dbCon.server_info()
except pymongo.errors.ServerSelectionTimeoutError as err:
    print " * Could not connect to DB: %s" % err

db = dbCon[cfg['db']]
collection = db[cfg['collection']]


@app.route('/')
def index():
    ''' POST GET '''
    # Checks if special mode is enabled
    dogeMode = {'value' : cfg['dogeMode']}
    # Query database for entries
    query = collection.find().sort("id", -1).limit(cfg['frontpageLimit'])
    return render_template('index.html', entries=query, dogeMode=dogeMode)

@app.errorhandler(404)
def page_not_found(error):
    ''' Error handling for 404 '''
    return render_template('404.html', err=error), 404


if __name__ == "__main__":
    app.run(debug=True, port=5100, host='0.0.0.0')

