# Finished Job Entry System (FJES) application 

Built with flask, pymongo, pyyaml,  

Installation:

1. Clone from git

2. cd entrypoint-app/ 

3. pip install -r requirements.txt

4. chmod +x entrypoint-app.py

5. ./entrypoint-app.py

## Starting

Simply start it by doing 
	
	cd database-app
	./app

Recommended to use with pm2 or supervisord. 
